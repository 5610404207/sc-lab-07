
public class Seat {
	private int time;
	private char row;
	private int column;

	public Seat(int time, char row, int column){
		this.time = time;
		this.row = row;
		this.column = column;
	}
	public Seat(char row2, int column2) {
		this.row = row2;
		this.column = column2;
	}
	public boolean equals(Object other){
		if (other == null)return false;
		if (!(other instanceof Seat))return false;
		Seat otherSeat = ((Seat) other);
		if (otherSeat.time == this.time
				&& otherSeat.row == this.row
				&& otherSeat.column == this.column){
			return true;
		}
		return false;
		}
	
	public String ToString(){
		return row+Integer.toString(column)+"round"+time;
	}
	public int getRow() {
		return this.row;
	}
	public int getColumn() {
		return this.column;
	}
	public int getTime() {
		return this.time;
	}
}